package di.iot.container.util.ws;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import di.iot.container.util.Constants;
import di.iot.container.util.ObjectMapperUtil;
import di.iot.container.util.errorhandling.UserError;
import di.iot.container.util.ws.beans.ApiLoginRequest;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class WSLoginService {
    private static final Logger log = LoggerFactory.getLogger(WSLoginService.class);

    private String username = "pgapi";
    private String password = "zC35^XGV";
    private String pgLogin = "https://dev-planandgo.di.no/pl/api/v-1/auth/generate";

    private static Cache<String, String> cachedJWTTokens = CacheBuilder
            .newBuilder()
            .expireAfterWrite( Constants.TOKEN_VALID_MINUTES, TimeUnit.MINUTES) // Invalidate tokens after x minutes.
            .build();

    public String getJWTToken() {
        try {
            return cachedJWTTokens.get(username, () -> {
                try {
                    return getValidToken();
                } catch (UserError userError) {
                    throw new Exception(userError);
                }
            });
        } catch (ExecutionException e) {
            log.warn(e.getMessage(), e);
        }
        return null;
    }

    public void invalidateJWTToken(){
        log.info("Invalidating JWT for user='" +  this.username + "'");
        cachedJWTTokens.invalidate(this.username);
    }

    private String getValidToken() throws UserError, IOException {
        return getValidToken(username, password);
    }

    public String getValidToken(String username, String password) throws UserError, IOException {
        Client client = ClientBuilder.newClient()
                .property(ClientProperties.CONNECT_TIMEOUT, Constants.REST_CONNECTION_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, Constants.REST_READ_TIMEOUT);
        WebTarget target = client.target(pgLogin);

        ApiLoginRequest loginRequest = ApiLoginRequest.builder()
                .username(username)
                .password(password)
                .build();

        Response response =
                target.request(MediaType.APPLICATION_JSON_TYPE)
                        .post(Entity.entity(loginRequest, MediaType.APPLICATION_JSON));

        if (response.getStatus() != 200) {
            throw new UserError(Response.Status.INTERNAL_SERVER_ERROR, "Unable to authenticate");
        }

        String jsonString = response.readEntity(String.class);
        ObjectNode node = ObjectMapperUtil.getDefaultObjectMapper().readValue(jsonString, ObjectNode.class);

        return node.get("token").asText("");
    }
}
