package di.iot.container.util.ws.beans;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ApiLoginRequest {
    private String username;
    private String password;
}
