package di.iot.container.util;

import java.util.*;

public class Constants {
    public static final int REST_CONNECTION_TIMEOUT = 5000;                             // 5 seconds
    public static final int REST_READ_TIMEOUT = 10000;                                  // 10 seconds
    public static final int TOKEN_VALID_MINUTES = 1440;                                 // 1 day in minutes

    // JWT Token constants
    // Default Token Secret
    public static final String DEFAULT_TOKEN_SECRET = "5(9*EX3evf-CV#$h";
    // Token duration
    public static final Integer CONSUMER_TOKEN_EXPIRATION_TIME = 3600;                  // 1 hour in seconds.
    public static final Integer RETAILER_TOKEN_EXPIRATION_TIME = 57600;                 // 16 hours in seconds.
    // Issuer
    public static final String TOKEN_ISSUER_ACCESS = "ship2me:access";
    public static final String TOKEN_ISSUER_REFRESH = "ship2me:refresh";
    public static final String TOKEN_ISSUER_RETAILER = "ship2me:retailer";
    // Claims
    public static final String TOKEN_CLAIM_USER_ID = "userId";                          // Cast to Integer
    public static final String TOKEN_CLAIM_CONTEXT_ID = "contextId";                    // Cast to Integer
    public static final String TOKEN_CLAIM_SUBSCRIPTION_PLAN_ID = "subscriptionPlanId"; // Cast to Integer
    public static final String TOKEN_CLAIM_PAID_UNTIL_DATE = "paidUntilDate";           // new Date(cast to long)
    public static final String TOKEN_CLAIM_USERNAME = "username";                       // Cast to String
    public static final String TOKEN_CLAIM_RETAILER_IDS = "retailerIds";                // Cast to List<Integer>

    // Package
    public static final Integer PACKAGE_PICKUP_CODE_LENGTH = 4;                         // pickup code length
    public static final Integer PACKAGE_PICKUP_DEADLINE_DAYS = 14;                      // amt. of days the retailer should keep a package before returning it to sender.
    public static final Integer PACKAGE_FILTER_MONTHS_DELIVERED = 12;                   // number of days after pickup we will continue to display packages.

    // Supported package event log languages
    public static final List<String> supportedLanguages;
    static {
        List<String> tmpList = new ArrayList<>();
        tmpList.add("NO");
        tmpList.add("EN");
        supportedLanguages = Collections.unmodifiableList(tmpList);
    }

    // Delivery time estimate hardcoded range values - 1-3 workdays to deliver
    public static final int PACKAGE_DELIVERY_DAYS_MINIMUM_TO_DELIVER = 1;
    public static final int PACKAGE_DELIVERY_DAYS_ESTIMATED_DIFFERENCE = 2;

    public static final int FRAM_CONTEXT_ID = 1;
    public static final int FRAM_SHIPSYSTEM_ID = 1;
    public static final int FRAM_EXTERNAL_SUBSCRIPTION_PLAN_ID = 1;     // TODO After MVP Make external include subscriptionPlanId
    public static final int FRAM_BASIC_SUBSCRIPTION_PLAN_ID = 1;

    public static final int mockFramShipmentC2CGoodyId = 1;
    public static final int mockFramReturnC2BGoodyId = 2;

    public static final int FRAM_SUBSCRIBE_SHOP_ID = 9;                 // PS: Not a Flow Shop, but a "DI Subscribe" Shop

    public static final int FRAM_SHIPMENT_SHOP_ID = 261;                // Using FRAM's own shop for shipments
    public static final int FRAM_SHIPMENT_TRANSPORT_SOLUTION_ID = 23;   // Using standard "MegtilDeg"
    public static final int FRAM_RETURN_SHOP_ID = 261;                  // Using FRAM's own shop for returns
    public static final int FRAM_RETURN_TRANSPORT_SOLUTION_ID = 21;     // Using standard "helthjem retur"

}

