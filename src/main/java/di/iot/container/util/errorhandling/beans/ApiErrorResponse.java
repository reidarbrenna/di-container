package di.iot.container.util.errorhandling.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(Include.NON_NULL)
public class ApiErrorResponse {
    private Integer status;
    private String error;
    private String message;
    private String timestamp;
    private String reference;

    public ApiErrorResponse(Integer status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }
}
