package di.iot.container.util.errorhandling;

import di.iot.container.util.errorhandling.beans.ApiErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    Logger logger = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(UserError.class)
    public ResponseEntity<ApiErrorResponse> handleUserErrorException(UserError userError, ServletWebRequest request) {
        // Create Response object to be returned
        ApiErrorResponse response = new ApiErrorResponse(userError.getStatusCode().getStatusCode(),userError.getStatusCode().toString(), userError.getMessage());
        response = addTraceDetails(response);

        // Log Response object to ease debugging
        logErrorResponse(response, userError, request);

        // Return response
        return new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatus()));
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ApiErrorResponse> handleException(Throwable throwable, ServletWebRequest request) {
        // Create Response object to be returned
        ApiErrorResponse response = new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR.name(),null);
        response = addTraceDetails(response);

        // Log Response object to ease debugging
        logErrorResponse(response, throwable, request);

        // Return response
        return  new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatus()));
    }

    private ApiErrorResponse addTraceDetails(ApiErrorResponse response) {
        if (response.getReference() == null) {
            response.setReference(UUID.randomUUID().toString());
        }

        if (response.getTimestamp() == null) {
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss X");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
            response.setTimestamp(dateFormatGmt.format(new Date()));
        }

        return response;
    }

    private void logErrorResponse(ApiErrorResponse response, Throwable throwable, ServletWebRequest request) {
        int statusCode = response.getStatus();
        String msg  = "HTTP " + statusCode
                    + ", " + response.getError()
                    + ", URI=" + request.getRequest().getRequestURI()
                    + (response.getMessage() == null ? "" : ", message=" + response.getMessage())
                    + ", timestamp=" + response.getTimestamp()
                    + ", ref=" + response.getReference();

        if (statusCode >= 500) {
            logger.error(msg,throwable);
        } else if (statusCode == 499 || statusCode == 408) {
            logger.warn(msg, throwable);
        } else if (statusCode >= 400) {
            logger.info(msg);
        } else if (statusCode >= 100) {
            logger.trace(msg, throwable);
        } else {
            logger.error(msg, throwable);
        }
    }
}
