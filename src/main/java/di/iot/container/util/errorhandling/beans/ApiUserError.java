package di.iot.container.util.errorhandling.beans;

import lombok.Data;

@Data
public class ApiUserError {
    private Integer statusCode;
    private String errorKey;
    private Object errorMap; // Not used for now
}
