package di.iot.container.util.errorhandling;

import javax.ws.rs.core.Response;

//@NotThreadSafe
public class UserError extends Exception {
    private Response.Status statusCode;


    public UserError(Response.Status statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public Response.Status getStatusCode() {
        return statusCode;
    }
}
