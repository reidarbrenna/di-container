package di.iot.container.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import di.iot.container.util.errorhandling.UserError;
import di.iot.container.util.errorhandling.beans.ApiUserError;
import di.iot.container.util.ws.WSLoginService;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;

@Service
public class JaxRsClientHelper {

    private WSLoginService wsLoginService;

    @Autowired
    public JaxRsClientHelper(WSLoginService wsLoginService) {
        this.wsLoginService = wsLoginService;
    }

    public Response doGet(String targetUrl, Map<String, Object> queryParams, String onBehalfOfUsername) throws UserError, IOException {
        Invocation.Builder builder = getDefaultBuilder(targetUrl, queryParams, onBehalfOfUsername);
        Response response = builder.get();

        if (response.getStatus() == HttpStatus.UNAUTHORIZED.value() ||
                response.getStatus() == HttpStatus.FORBIDDEN.value()) {
            // invalidate token and try again
            wsLoginService.invalidateJWTToken();
            builder = getDefaultBuilder(targetUrl, queryParams, onBehalfOfUsername);
            response = builder.get();
        }

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            deserializeErrorResponse(response);
        }

        return response;
    }

    public Response doPost(String targetUrl, Entity entity, Map<String, Object> queryParams, String onBehalfOfUsername) throws IOException, UserError {
        Invocation.Builder builder = getDefaultBuilder(targetUrl, queryParams, onBehalfOfUsername);
        Response response = builder.post(entity);

        if (response.getStatus() == HttpStatus.UNAUTHORIZED.value() ||
                response.getStatus() == HttpStatus.FORBIDDEN.value()) {
            // invalidate token and try again
            wsLoginService.invalidateJWTToken();
            builder = getDefaultBuilder(targetUrl, queryParams, onBehalfOfUsername);
            response = builder.post(entity);
        }

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            deserializeErrorResponse(response);
        }

        return response;
    }

    public Response doPut(String targetUrl, Entity entity, String onBehalfOfUsername) throws UserError, IOException {
        Invocation.Builder builder = getDefaultBuilder(targetUrl, null, onBehalfOfUsername);
        Response response = builder.put(entity);

        if (response.getStatus() == HttpStatus.UNAUTHORIZED.value() ||
                response.getStatus() == HttpStatus.FORBIDDEN.value()) {
            // invalidate token and try again
            wsLoginService.invalidateJWTToken();
            builder = getDefaultBuilder(targetUrl, null, onBehalfOfUsername);
            response = builder.put(entity);
        }

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            deserializeErrorResponse(response);
        }

        return response;
    }

    private Entity getBlankEntity() {
        return Entity.entity("", MediaType.APPLICATION_JSON_TYPE);
    }

    private void deserializeErrorResponse(Response response) throws UserError, IOException {
        String jsonString = response.readEntity(String.class);
        ObjectMapper mapper = ObjectMapperUtil.getDefaultObjectMapper();
        ApiUserError error = mapper.readValue(jsonString, ApiUserError.class);
        throw new UserError(Response.Status.fromStatusCode(response.getStatus()), error.getErrorKey());
    }

    private Invocation.Builder getDefaultBuilder(String targetUrl, Map<String, Object> queryParams, String onBehalfOfUsername) {
        WebTarget webTarget = ClientBuilder.newClient()
                .property(ClientProperties.CONNECT_TIMEOUT, Constants.REST_CONNECTION_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, Constants.REST_READ_TIMEOUT)
                .target(targetUrl);
        // Set queryParameters
        if (queryParams != null) {
            for (Map.Entry queryParam : queryParams.entrySet()) {
                webTarget = webTarget.queryParam((String) queryParam.getKey(), queryParam.getValue());
            }
        }
        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header("x-auth-token", wsLoginService.getJWTToken());

        if (onBehalfOfUsername != null) {
            return builder.header("X-On-Behalf-Of", onBehalfOfUsername);
        }
        return builder;
    }
}
