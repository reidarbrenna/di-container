package di.iot.container.api.ping;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/ping", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PingController {

    private final Jdbi jdbi;

    @Autowired
    public PingController(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @GetMapping()
    @ApiOperation(
            value = "Ping the application"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "pong"),
            }
    )
    public ResponseEntity<String> ping() {
        return ResponseEntity.status(200).body("pong");
    }

    @GetMapping("db")
    @ApiOperation(
            value = "Ping the application database"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "pongfromdb"),
            }
    )
    public ResponseEntity<String> pingDb() {
        return ResponseEntity.status(200).body(jdbi.withHandle(h -> h.select("select 'pongfromdb' as response").mapTo(String.class).findFirst().orElse("fail")));
    }
}
