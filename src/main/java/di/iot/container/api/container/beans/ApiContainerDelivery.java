package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainerDelivery {
    private String product; //container type
    private Integer productCount; //volume * load
    private Boolean pickup = true;

    public static ApiContainerDelivery fromContainerInfo(ContainerInfo containerInfo) {
        return new ApiContainerDelivery().toBuilder()
                .product(containerInfo.getTypeName())
                .productCount((containerInfo.getVolume() * containerInfo.getLoadPercent())/100)
                .build();
    }
}
