package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainerStartLocation {
    private List<ApiContainerRoute> routes;
    private String address = "Akersgata 55";
    private Integer zip = 180;
    private String city = "Oslo";
    private String country = "NO";
    private double latitude = 59.915355;
    private double longitude = 10.7410316;

    public static ApiContainerStartLocation fromContainerInfo(List<ContainerInfo> containerInfoList) {

        return new ApiContainerStartLocation().toBuilder()
                .routes(Arrays.asList(ApiContainerRoute.fromContainerInfo(containerInfoList)))
                .build();
    }
}
