package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainerStop {
     String address;
     Integer zip;
     String city;
     String country;
     Integer routeSequence; //counter
     Integer duration = 300;
     Double latitude;
     Double longitude;
     List<ApiContainerCustomer> customers;

    public static ApiContainerStop fromContainerInfo(ContainerInfo containerInfo, Integer customerSystemId, Integer sequenceNumber) {
        return new ApiContainerStop().toBuilder()
                .address(containerInfo.getAddress())
                .zip(containerInfo.getZip())
                .city(containerInfo.getCity())
                .country(containerInfo.getCountry())
                .routeSequence(sequenceNumber)
                .latitude(containerInfo.getLatitude())
                .longitude(containerInfo.getLongitude())
                .customers(Arrays.asList(ApiContainerCustomer.fromContainerInfo(containerInfo, customerSystemId)))
                .build();
    }
}
