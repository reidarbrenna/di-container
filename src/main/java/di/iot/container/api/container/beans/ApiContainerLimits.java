package di.iot.container.api.container.beans;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ApiContainerLimits {
    private String start = "15:30";
    private String stop = "20:30";
    private Integer routeCount = 1;
    private Boolean returnToPickupLocation = false;
    private Boolean cluster = true;
}
