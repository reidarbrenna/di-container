package di.iot.container.api.container;

import di.iot.container.api.container.beans.ApiContainer;
import di.iot.container.api.container.beans.ApiSensorTransmission;
import di.iot.container.core.container.service.ContainerService;
import di.iot.container.util.errorhandling.UserError;
import di.iot.container.core.exportservice.ExportService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/container", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ContainerController {

    @Autowired
    private ExportService exportService;

    @Autowired
    private ContainerService containerService;

    @PostMapping()
    @ApiOperation(
            value = "Endpoint to feed db with container information"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "data received"),
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public void addContainerData(@ApiParam(value = "The sensor transmission", required = true) @RequestBody ApiSensorTransmission transmission) {
        containerService.registerTransmission(transmission);
    }

    @GetMapping()
    @ApiOperation(
            value = "Endpoint to export data to plan and go"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "data exported"),
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public ApiContainer getContainerData(
            @ApiParam(required = true) @QueryParam("export") Boolean export
    ) throws UserError {
        ApiContainer containerData = containerService.getAllContainers();

        if (export != null && export) {
            try {
                exportService.exportContainerData(containerData);
            } catch (IOException e) {
                throw new UserError(Response.Status.INTERNAL_SERVER_ERROR, "error.error.error");
            }
        }

        return containerData;
    }
}
