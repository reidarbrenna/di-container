package di.iot.container.api.container.beans;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiSensorTransmission {
    private String type;
    private ApiSensorDevice device;
    private String payload;
    private Long received;
}
