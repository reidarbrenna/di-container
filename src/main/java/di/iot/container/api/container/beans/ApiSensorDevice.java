package di.iot.container.api.container.beans;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiSensorDevice {
    private String deviceId;
    private String collectionId;
    private String imei;
    private String imsi;
    private ApiSensorTag tags;
}