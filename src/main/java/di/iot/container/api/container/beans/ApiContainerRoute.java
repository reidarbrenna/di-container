package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainerRoute {
            private String name = "DumpsterPickupOslo";
            private Integer driverId = 35211;
            private String start = "15:30";
            private Boolean startDayBefore = false;
            private Boolean geoFencing = true;
            private List<ApiContainerStop> stops;

    public static ApiContainerRoute fromContainerInfo(List<ContainerInfo> containerInfoList) {
        List<ApiContainerStop> stops = new ArrayList<>();
        Integer counter = 0;
        for (ContainerInfo info: containerInfoList) {
           stops.add(ApiContainerStop.fromContainerInfo(info, info.getCustomerSystemId(), counter));
           counter++;
        }
        return new ApiContainerRoute().toBuilder()
                .stops(stops)
                .build();
    }
}
