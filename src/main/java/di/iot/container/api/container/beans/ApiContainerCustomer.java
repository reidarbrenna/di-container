package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainerCustomer {
    private Integer customerSystemId;
    private String name;
    private String customerType = "B";
    private List<ApiContainerDelivery> deliveries;

    public static ApiContainerCustomer fromContainerInfo(ContainerInfo containerInfo, Integer customerSystemId) {
        return new ApiContainerCustomer().toBuilder()
                .customerSystemId(customerSystemId)
                .name(containerInfo.getName())
                .deliveries(Arrays.asList(ApiContainerDelivery.fromContainerInfo(containerInfo)))
                .build();
    }
}
