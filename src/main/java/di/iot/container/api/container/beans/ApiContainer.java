package di.iot.container.api.container.beans;

import di.iot.container.core.container.beans.ContainerInfo;
import lombok.*;
import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class ApiContainer {
    private Integer companyId;
    private String date;
    private Integer distrNo = 0;
    private String name = "RedCrossDumpsters";
    private Boolean showLoadList = false;
    private ApiContainerLimits limits = new ApiContainerLimits();
    private Boolean autoPublish = true;
    private List<ApiContainerStartLocation> pickupLocations;



    public static ApiContainer fromContainerInfo(List<ContainerInfo> containerInfoList, Integer companyId) {
        return new ApiContainer().toBuilder()
                .companyId(companyId)
                .date("2018-10-19")
                .pickupLocations(Arrays.asList(ApiContainerStartLocation.fromContainerInfo(containerInfoList)))
                .build();
    }
}
