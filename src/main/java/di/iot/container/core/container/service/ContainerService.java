package di.iot.container.core.container.service;

import di.iot.container.api.container.beans.ApiContainer;
import di.iot.container.api.container.beans.ApiSensorTransmission;
import di.iot.container.core.container.beans.ContainerInfo;
import di.iot.container.core.container.dao.ContainerDAO;
import org.glassfish.jersey.internal.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ContainerService {

    @Autowired
    private ContainerDAO containerDAO;

    public void registerTransmission(ApiSensorTransmission transmission) {
        String sensorId = transmission.getDevice().getDeviceId();
        Integer registeredHeight = Integer.valueOf(new String(Base64.decode(transmission.getPayload().getBytes())));
        ContainerInfo info = containerDAO.getContaierInfoFromSensorId(sensorId);
        Integer newLoadPercent = getPercent(registeredHeight,info.getHeight());

        if (newLoadPercent != info.getLoadPercent()) {
            containerDAO.insertNewLoad(info.getId(), new Date(transmission.getReceived()), newLoadPercent);
        }
    }

    public ApiContainer getAllContainers() {
        List<ContainerInfo> containerInfoList = containerDAO.getContaierInfoForAllContainers();

       if (containerInfoList == null || containerInfoList.isEmpty()) {
           return null;
       }
       return ApiContainer.fromContainerInfo(containerInfoList,containerInfoList.get(0).getCompanyId());
    }

    private int getPercent(int registeredHeight, int totalHeight) {
        float proportion = (float) registeredHeight / totalHeight;
        proportion = proportion * 100;

        return (int) (100 - proportion);
    }
}
