package di.iot.container.core.container.dao;

import di.iot.container.core.container.beans.ContainerInfo;
import di.iot.container.core.container.rowmappers.ContainerInfoRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

@Repository
public class ContainerDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ContainerDAO(@Qualifier("dataSource") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public ContainerInfo getContaierInfoFromSensorId(String sensorId) {
        String sql = "select distinct on (c.id)" +
                " c.id, " +
                " c.name, " +
                " c.company_id, " +
                " c.customer_system_id, " +
                " c.address, " +
                " c.zip, " +
                " c.city, " +
                " c.country, " +
                " c.latitude, " +
                " c.longitude, " +
                " cl.load_percent, " +
                " ct.height, " +
                " ct.width, " +
                " ct.depth, " +
                " ct.volume, " +
                " ct.name as type_name" +
                " from container c " +
                " join container_type ct " +
                " on c.type_id = ct.id " +
                " join container_load cl " +
                " on c.id = cl.container_id " +
                " where c.sensor_id = ? " +
                " order by c.id, cl.timestamp desc";

        return jdbcTemplate.queryForObject(sql, new ContainerInfoRowMapper(), sensorId);
    }

    public List<ContainerInfo> getContaierInfoForAllContainers() {
        String sql = "select distinct on (c.id)" +
                " c.id, " +
                " c.name, " +
                " c.company_id, " +
                " c.customer_system_id, " +
                " c.address, " +
                " c.zip, " +
                " c.city, " +
                " c.country, " +
                " c.latitude, " +
                " c.longitude, " +
                " cl.load_percent, " +
                " ct.height, " +
                " ct.width, " +
                " ct.depth, " +
                " ct.volume, " +
                " ct.name as type_name" +
                " from container c " +
                " join container_type ct " +
                " on c.type_id = ct.id " +
                " join container_load cl " +
                " on c.id = cl.container_id " +
                " order by c.id, cl.timestamp desc";

        return jdbcTemplate.query(sql, new ContainerInfoRowMapper());
    }

    public void insertNewLoad(Integer containerId, Date registerdDate, Integer loadPercent) {
        String sql = "insert into container_load(container_id, \"timestamp\", load_percent) values(?, ?, ?)";

        jdbcTemplate.update(sql, containerId, registerdDate, loadPercent);
    }
}
