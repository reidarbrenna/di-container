package di.iot.container.core.container.beans;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ContainerInfo {
    private Integer id;
    private String name;
    private Integer companyId;
    private Integer customerSystemId;
    private String address;
    private Integer zip;
    private String city;
    private String country;
    private Double latitude;
    private Double longitude;
    private Integer loadPercent;
    private Integer height;
    private Integer width;
    private Integer depth;
    private Integer volume;
    private String typeName;
}
