package di.iot.container.core.container.rowmappers;

import di.iot.container.core.container.beans.ContainerInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ContainerInfoRowMapper implements RowMapper<ContainerInfo> {
    @Override
    public ContainerInfo mapRow(ResultSet rs, int i) throws SQLException {

        return ContainerInfo.builder()
                .id(rs.getInt("id"))
                .name(rs.getString("name"))
                .companyId(rs.getInt("company_id"))
                .customerSystemId(rs.getInt("customer_system_id"))
                .address(rs.getString("address"))
                .zip(rs.getInt("zip"))
                .city(rs.getString("city"))
                .country(rs.getString("country"))
                .latitude(rs.getDouble("latitude"))
                .longitude(rs.getDouble("longitude"))
                .loadPercent(rs.getInt("load_percent"))
                .height(rs.getInt("height"))
                .width(rs.getInt("width"))
                .depth(rs.getInt("depth"))
                .volume(rs.getInt("volume"))
                .typeName(rs.getString("type_name"))
                .build();
    }
}