package di.iot.container.core.exportservice;

import di.iot.container.api.container.beans.ApiContainer;
import di.iot.container.util.JaxRsClientHelper;
import di.iot.container.util.errorhandling.UserError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Service
public class ExportService {

    @Autowired
    private JaxRsClientHelper jaxRsClientHelper;

    private static final String targetUrl = "https://dev-planandgo.di.no/pl/api/importer/v-1/routePlan";

    public void exportContainerData(ApiContainer container) throws UserError, IOException {
        if (container != null) {
            Entity entity = Entity.entity(container, MediaType.APPLICATION_JSON_TYPE);
            jaxRsClientHelper.doPost(targetUrl, entity, null, null);
        }
    }
}
